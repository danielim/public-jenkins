# FROM nginx
# COPY . /usr/share/nginx/html

# EXPOSE 80

FROM node
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
CMD ["npm", "start"]